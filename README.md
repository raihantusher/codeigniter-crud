

codeigniter crud By Raihan Tusher
==================
This application is written on the top of codeigniter 3.1.8.

How to use:
===============
- create a database
- import sql file
- put assets folder where index.php file 
- then put files controller, model, view folder as usual

links:
===============
- view url http://localhost/[app-home]/index.php/main/index
- create url http://localhost/[app-home]/index.php/main/create

It is easy to reuse and customize.
==================================