<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
			// load db and model
		parent::__construct();
        $this->load->database();
        $this->load->library("Pagination");

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->helper("url");

        $this->load->model('Crud');
     
	}
	



	public function index()
	{
        // init params
        $params = array();
        $limit_per_page = 5;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $total_records = $this->Crud->get_total();
       
        if ($total_records > 0)
        {
            // get current page records
             $params["results"] = $this->Crud->get_current_page_records($limit_per_page, $page*$limit_per_page);
       
                 
            $config['base_url'] = base_url() . 'main/index';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;
             
            // custom paging configuration
            $config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;
             
            $config['full_tag_open'] = '<div class="pagination">';
            $config['full_tag_close'] = '</div>';
             
            $config['first_link'] = 'First Page';
            $config['first_tag_open'] = '<span class="firstlink">';
            $config['first_tag_close'] = '</span>';
             
            $config['last_link'] = 'Last Page';
            $config['last_tag_open'] = '<span class="lastlink">';
            $config['last_tag_close'] = '</span>';
             
            $config['next_link'] = 'Next Page';
            $config['next_tag_open'] = '<span class="nextlink">';
            $config['next_tag_close'] = '</span>';
 
            $config['prev_link'] = 'Prev Page';
            $config['prev_tag_open'] = '<span class="prevlink">';
            $config['prev_tag_close'] = '</span>';
 
            $config['cur_tag_open'] = '<span class="curlink">';
            $config['cur_tag_close'] = '</span>';
 
            $config['num_tag_open'] = '<span class="numlink">';
            $config['num_tag_close'] = '</span>';
             
            $this->pagination->initialize($config);
                 
            // build paging links
            $params["links"] = $this->pagination->create_links();



	}

		$this->load->view("header");
		$this->load->view('crud/read',$params);
		$this->load->view('footer');
 }




 public function create(){
 	$data=array();
 	$data["message"]="";
 	$this->form_validation->set_rules('name', 'Title', 'required');
    $this->form_validation->set_rules('roll', 'Text', 'required');


     if ($this->form_validation->run() === FALSE)
    {
    	
    }else{
    	if($this->Crud->insert()){
    		$data["message"]='<div class="alert alert-success" role="alert">
  Database has been updated successfully!
</div>';

    	}
    }
 	$this->load->view("header");
 	$this->load->view("crud/create",$data);
 	$this->load->view("footer");
 }


 public function edit(){
 	    $var=$_GET['id'];
 	    $data=array();
 	 	$data['db']=$this->Crud->student($var);

 	 	$data["message"]="";


 	$this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('roll', 'required', 'required');


     if ($this->form_validation->run() === FALSE)
    {
    	
    }else{
    	if( $this->Crud->update($var))

 	 	$data["message"]='<div class="alert alert-success" role="alert">
  Data updated successfully! Go to main page!
</div>';

    }
       


 	 	$this->load->view("header");
 	 	$this->load->view("crud/update",$data);
 	 	$this->load->view("footer");
 	 
  }

  public function delete(){
  	$data=array();
  	$data["message"]='';

  	$id=$this->input->get("id");
  	if($this->Crud->delete($id)){

  		$data["message"]='<div class="alert alert-success" role="alert"> ID 
  			'.$id.' is deleted successfully!
			</div>';
  	}



  

 	 	$this->load->view("header");
 	 	$this->load->view("crud/delete",$data);
 	 	$this->load->view("footer");
 	 }
}
