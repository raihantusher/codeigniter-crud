<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Ion Auth Model
 * @property Bcrypt $bcrypt The Bcrypt library
 * @property Ion_auth $ion_auth The Ion_auth library
 */
class Crud extends CI_Model
{

 function __construct() 
    {
      parent::__construct();
        
    }

    public function insert(){
                $data = array(
                'name' => $this->input->post('name'),
                'roll'=>$this->input->post('roll')
            );

           return $this->db->insert('student', $data);
    }

    public function update($id){
                
           $this->db->set("name",$this->input->post('name'));
           $this->db->set("roll",$this->input->post("roll"));
           $this->db->where('id', $id);

           return $this->db->update('student');
    }




    public function delete($id){
        $this->db->where('id', $id);
        return $this->db->delete('student');
    }




    public function student($id=false){
        
        if($id>0)
        {
            $this->db->select('*');
            $this->db->from('student');
            $this->db->where('id',$id);
            $query=$this->db->get();
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
        }
        
    }


 
    public function get_current_page_records($limit, $start) 
    {

        $filter=$this->input->post("filter");
        $search=$this->input->post("search");
  

            switch ($filter) {
                case 'name':
                    # code...
                    $this->db->like('name', $search);
                    break;
                case 'roll':
                    # code...
                    $this->db->like('roll',$search);
                    break;
                
                default:
                    # code...
                $this->db->like('name', $search); 
                $this->db->or_like('roll', $search);
                    break;
            }
            
        

        //$this->db->limit($limit, $start);
        $query = $this->db->get("student",$limit,$start);
       // $query=$this->db->get("student");

 
        if ($query->num_rows() > 0) 
        {
            foreach ($query->result() as $row) 
            {
                $data[] = $row;
            }
             
            return $data;
        }
 
       
    }
     
    public function get_total() 
    {

        $filter=$this->input->post("filter");
        $search=$this->input->post("search");
        
            
             switch ($filter) {
                case 'name':
                    # code...
                    $this->db->like('name', $search);
                    break;
                case 'roll':
                    # code...
                    $this->db->like('roll',$search);
                    break;
                
                default:
                    # code...
                    $this->db->like('name', $search); 
                    $this->db->or_like('roll', $search);
                
                    break;
            }
        
        $query=$this->db->get("student");
        return $query->num_rows();
    }
}


