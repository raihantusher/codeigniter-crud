
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
             

                <form method="post"  action="<?=base_url()?>main/index">
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInput">Name</label>
      <input type="text" class="form-control mb-2" id="inlineFormInput" name="search">
    </div>
    

    <div class="col-auto">
      <div class="form-check mb-2">
      <select class="form-control" name="filter">
                <option>none</option>
                <option>name</option>
                <option>roll</option>
                
              </select>
      </div>
    </div>
    <div class="col-auto">
      <button type="submit" class="btn btn-primary mb-2">Submit</button>
    </div>
  </div>
</form>


               <?php if (isset($results)) { ?>
                <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">#ID</th>
                          <th scope="col">Name</th>
                          <th scope="col">Roll</th>
                          <th scope="col">Handle</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($results as $data) { ?>
                        <tr>
                          <th ><?=$data->id?></th>
                          <td><?=$data->name?></td>
                          <td><?=$data->roll?></td>
                            <td> <a href="<?=base_url()?>main/edit?id=<?=$data->id?>" class="btn btn-primary" >Edit</a> <a href="<?=base_url()?>main/delete?id=<?=$data->id?>"class="btn btn-danger">Delete</button></td>
                        </tr>
              

                         <?php } ?>

                      </tbody>
                    </table>
                
                </table>

                 <?php } else { ?>
                <div>No user(s) found.</div>
            <?php } ?>
 
            <?php if (isset($links)) { ?>
              <nav aria-label="Page navigation example">
                <?php echo $links ?>
              </nav>
            <?php } ?>
            
            
            </div>
        
        </div>
    
    </div>
    
   
                